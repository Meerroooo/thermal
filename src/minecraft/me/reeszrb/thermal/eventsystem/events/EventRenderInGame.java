package me.reeszrb.thermal.eventsystem.events;

import me.reeszrb.thermal.eventsystem.Event;
import net.minecraft.client.gui.FontRenderer;

public class EventRenderInGame extends Event{
	private FontRenderer fr;
	
	public void setFontRenderer(FontRenderer fr){
		this.fr = fr;
	}
	
	public FontRenderer getFontRenderer(){
		return fr;
	}
}
