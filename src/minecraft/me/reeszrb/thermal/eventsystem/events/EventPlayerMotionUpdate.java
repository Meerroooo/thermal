package me.reeszrb.thermal.eventsystem.events;

import me.reeszrb.thermal.eventsystem.EnumState;
import me.reeszrb.thermal.eventsystem.Event;

public class EventPlayerMotionUpdate extends Event{
	private EnumState state;
	
	public EventPlayerMotionUpdate(EnumState state) {
		this.state = state;
	}
	
	public EnumState getState() {
		return state;
	}
}
