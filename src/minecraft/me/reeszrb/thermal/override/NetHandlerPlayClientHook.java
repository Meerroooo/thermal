package me.reeszrb.thermal.override;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;

public class NetHandlerPlayClientHook extends NetHandlerPlayClient{

	public NetHandlerPlayClientHook(Minecraft p_i45061_1_,
			GuiScreen p_i45061_2_, NetworkManager p_i45061_3_) {
		super(p_i45061_1_, p_i45061_2_, p_i45061_3_);
	}

}
