package me.reeszrb.thermal.override;

import me.reeszrb.thermal.Wrapper;
import me.reeszrb.thermal.modulemanager.HookManager;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.Minecraft;
import net.minecraft.client.audio.PositionedSoundRecord;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.passive.EntityHorse;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSword;
import net.minecraft.network.play.client.C02PacketUseEntity;
import net.minecraft.network.play.client.C07PacketPlayerDigging;
import net.minecraft.network.play.client.C08PacketPlayerBlockPlacement;
import net.minecraft.network.play.client.C09PacketHeldItemChange;
import net.minecraft.network.play.client.C0EPacketClickWindow;
import net.minecraft.network.play.client.C10PacketCreativeInventoryAction;
import net.minecraft.network.play.client.C11PacketEnchantItem;
import net.minecraft.stats.StatFileWriter;
import net.minecraft.util.ChatComponentText;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.Vec3;
import net.minecraft.world.World;
import net.minecraft.world.WorldSettings;

public class PlayerController extends PlayerControllerMP{

	public PlayerController(Minecraft p_i45062_1_,
			NetHandlerPlayClient p_i45062_2_) {
		super(p_i45062_1_, p_i45062_2_);
	}
	
	public void attackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity)
    {
		HookManager.getInstance().preAttackEntity(par1EntityPlayer, par2Entity);
		HookManager.getInstance().onAttackEntity(par1EntityPlayer, par2Entity);
        super.attackEntity(par1EntityPlayer, par2Entity);
        HookManager.getInstance().postAttackEntity(par1EntityPlayer, par2Entity);
    }
	
	public void clickBlock(int par1, int par2, int par3, int par4){
		HookManager.getInstance().onClickBlock(par1, par2, par3, par4);
		Wrapper.getInstance().getBasicUtils().printToConsole("Hitted");
		super.clickBlock(par1, par2, par3, par4);
	}
	
}
