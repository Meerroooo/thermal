package me.reeszrb.thermal.override;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.EntityRenderer;
import net.minecraft.client.resources.IResourceManager;

public class EntityRendererHook extends EntityRenderer{

	public EntityRendererHook(Minecraft p_i45076_1_,
			IResourceManager p_i45076_2_) {
		super(p_i45076_1_, p_i45076_2_);
	}

}
