package me.reeszrb.thermal.override;

import me.reeszrb.thermal.Wrapper;
import me.reeszrb.thermal.eventsystem.EventManager;
import me.reeszrb.thermal.eventsystem.events.EventRenderInGame;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiIngame;

public class GuiInGame extends GuiIngame{

	public GuiInGame(Minecraft par1Minecraft) {
		super(par1Minecraft);
	}
	
	@Override
	public void renderGameOverlay(float par1, boolean par2, int par3, int par4){
		super.renderGameOverlay(par1, par2, par3, par4);
		Wrapper.getInstance().getFontRenderer().drawString("Thermal", 2, 2, 0xffffffff);
		EventRenderInGame renderingame = new EventRenderInGame();
		EventManager.fireEvent(renderingame);
		renderingame.setFontRenderer(Wrapper.getInstance().getFontRenderer());
	}

}
