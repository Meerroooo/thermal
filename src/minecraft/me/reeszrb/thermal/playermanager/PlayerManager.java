package me.reeszrb.thermal.playermanager;

import java.util.ArrayList;

import net.minecraft.util.StringUtils;

public class PlayerManager {
	private ArrayList<Object> playersList = new ArrayList<Object>();
	
	private static PlayerManager thePlayerManager;
	
	public static PlayerManager getInstance(){
		if(thePlayerManager == null) thePlayerManager = new PlayerManager();
		return thePlayerManager;
	}
	
	public ArrayList<Object> getPlayerList(){
		return playersList;
	}
	
	public void addPlayer(Player player)
	{
		playersList.add(player);
	}
	
	public void removePlayer(String name)
	{
		for(Object p: playersList)
		{
			if(p instanceof Friend){
				Friend f = (Friend)p;
				if(f.getName().equalsIgnoreCase(name))
				{
					playersList.remove(p);
					break;
				}
			}
			if(p instanceof Enemy){
				Enemy f = (Enemy)p;
				if(f.getName().equalsIgnoreCase(name))
				{
					playersList.remove(p);
					break;
				}
			}
		}
	}
	
	public boolean isFriend(String name){
		boolean isFriend = false;
		for(Object o: playersList)
		{
			if(o instanceof Friend){
				Friend f = (Friend)o;
				if(f.getName().equalsIgnoreCase(StringUtils.stripControlCodes(name)))
				{
					isFriend = true;
					break;
				}
			}
		}
		return isFriend;
	}
	
	public boolean isEnemy(String name){
		boolean isEnemy = false;
		for(Object o: playersList)
		{
			if(o instanceof Enemy){
				Enemy f = (Enemy)o;
				if(f.getName().equalsIgnoreCase(StringUtils.stripControlCodes(name)))
				{
					isEnemy = true;
					break;
				}
			}
		}
		return isEnemy;
	}
}
