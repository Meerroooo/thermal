
package me.reeszrb.thermal.playermanager;

public class Player {
	private String name;
	private String rename;
	
	public Player(String name, String rename) {
		this.name = name;
		this.rename = rename;
	}
	
	public final String getName() {
		return name;
	}
	
	public final String getRename() {
		return rename;
	}
	
	public final void setName(String s) {
		name = s;
	}
	
	public final void setAlias(String s) {
		rename = s;
	}
}
