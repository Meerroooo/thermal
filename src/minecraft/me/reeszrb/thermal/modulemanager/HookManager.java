package me.reeszrb.thermal.modulemanager;

import net.minecraft.client.gui.GuiScreen;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.network.play.server.S02PacketChat;

public class HookManager {
	
	private static HookManager theHookManager;
	
	public static HookManager getInstance(){
		if(theHookManager == null) theHookManager = new HookManager();
		return theHookManager;
	}
	
	public void onGameTick(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onGameTick();
			}
		}
	}
	
	public void preMotionUpdate(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.preMotionUpdate();
			}
		}
	}

	public void onMotionUpdate(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onMotionUpdate();
			}
		}
	}
	
	public void postMotionUpdate(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.postMotionUpdate();
			}
		}
	}
	
	public void preAttackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.preAttackEntity(par1EntityPlayer, par2Entity);
			}
		}
	}
	
	public void onAttackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onAttackEntity(par1EntityPlayer, par2Entity);
			}
		}
	}
	
	public void postAttackEntity(EntityPlayer par1EntityPlayer, Entity par2Entity){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.postAttackEntity(par1EntityPlayer, par2Entity);
			}
		}
	}
	
	public void onRender(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onRender();
			}
		}
	}
	
	public void onClickBlock(int i, int j, int k, int l){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onClickBlock( i, j, k, l);
			}
		}
	}
	
	public void onPlayerDeath(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onPlayerDeath();
			}
		}
	}
	
	public void onPlayerRespawn(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onPlayerRespawn();
			}
		}
	}
	
	public void onMiddleClick(){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				m.onMiddleClick();
			}
		}
	}
	
	public GuiScreen onGuiScreen(GuiScreen guiscreen){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				return m.onGuiScreen(guiscreen);
			}
		}
		return guiscreen;
	}
	
	public boolean onSendChatMessage(String s){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				return m.onSendChatMessage(s);
			}
		}
		return true;
	}
	
	public boolean onReceiveChatMessage(S02PacketChat packet){
		for(Module m: ModuleManager.getInstance().getModules()){
			if(m.isState(true)){
				return m.onReceiveChatMessage(packet);
			}
		}
		return true;
	}
}
