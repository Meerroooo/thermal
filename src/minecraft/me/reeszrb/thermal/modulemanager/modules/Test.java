package me.reeszrb.thermal.modulemanager.modules;

import me.reeszrb.thermal.eventsystem.Event;
import me.reeszrb.thermal.eventsystem.EventAnnotation;
import me.reeszrb.thermal.eventsystem.events.EventRenderInGame;
import me.reeszrb.thermal.modulemanager.Category;
import me.reeszrb.thermal.modulemanager.Module;

public class Test extends Module{

	public Test() {
		super("Hiest", "", 0, 0, Category.NONE, true);
		this.setState(true);
	}
	
	@EventAnnotation(event = EventRenderInGame.class)
	public void RenderIngame(){
		getFontRenderer().drawString("test", 2, 14, 0xffffffff);
	}
	
}
