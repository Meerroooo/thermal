package me.reeszrb.thermal.modulemanager;

import java.util.ArrayList;

import me.reeszrb.thermal.modulemanager.modules.Test;

public class ModuleManager {
	private ArrayList<Module> mods = new ArrayList<Module>();
	private static ModuleManager theModuleManager;
	
	public static ModuleManager getInstance(){
		if(theModuleManager == null) theModuleManager = new ModuleManager();
		return theModuleManager;
	}
	
	public ModuleManager(){
		initModules();
	}
	
	private void initModules() {
		getModules().add(new Test());
	}

	public ArrayList<Module> getModules(){
		return mods;
	}
	
	public Module getModule(Class <? extends Module> clazz){
		for(Module mod: getModules())
		{
			if(mod.getClass() == clazz)
			{
				return mod;
			}
		}
		
		return null;
	}
}
