package me.reeszrb.thermal;

import me.reeszrb.thermal.utils.BasicUtils;
import me.reeszrb.thermal.utils.EntityUtils;
import me.reeszrb.thermal.utils.GL2DUtils;
import me.reeszrb.thermal.utils.GL3DUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityClientPlayerMP;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.multiplayer.PlayerControllerMP;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.client.renderer.EntityRenderer;

public class Wrapper {
	
	private static Wrapper theWrapper;
	
	public static Wrapper getInstance(){
		if(theWrapper == null) theWrapper = new Wrapper();
		return theWrapper;
	}
	
	private BasicUtils bu = new BasicUtils();
	private EntityUtils eu = new EntityUtils();
	private GL2DUtils twodu = new GL2DUtils();
	private GL3DUtils threedu = new GL3DUtils();
	private Minecraft mc = Minecraft.getMinecraft();
	
	
	public BasicUtils getBasicUtils(){
		return bu; 
	}
	
	public EntityUtils getEntityUtils(){
		return eu;
	}
	
	public GL2DUtils get2DRenderUtils(){
		return twodu;
	}
	
	public GL3DUtils get3DRenderUtils(){
		return threedu;
	}
	
	public EntityClientPlayerMP getPlayer(){
		return mc.thePlayer;
	}
	
	public WorldClient getWorld(){
		return mc.theWorld;
	}
	
	public PlayerControllerMP getPlayerController(){
		return mc.playerController;
	}
	
	public EntityRenderer getEntityRenderer(){
		return mc.entityRenderer;
	}
	
	public FontRenderer getFontRenderer(){
		return mc.fontRenderer;
	}
	
	public Minecraft getMinecraft(){
		return mc;
	}
}
