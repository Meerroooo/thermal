package me.reeszrb.thermal.valuemanager;

import java.util.ArrayList;

public class ValueManager {
	private ArrayList<Value> vals = new ArrayList<Value>();
	
	public ArrayList<Value> getValues(){
		return vals;
	}
	
	public ValueManager(){
		initValues();
	}
	
	public void initValues(){
		
	}
	
	private static ValueManager theValueManager;
	
	public static ValueManager getInstance(){
		if(theValueManager == null) theValueManager = new ValueManager();
		return theValueManager;
	}
}
