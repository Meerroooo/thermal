package me.reeszrb.thermal.valuemanager;

public enum ValueType {
	INTEGER, DECIMAL, PERCENTAGE
}
