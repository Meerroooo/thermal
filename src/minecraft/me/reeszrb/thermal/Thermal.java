package me.reeszrb.thermal;

import me.reeszrb.thermal.configmanager.ConfigurationManager;
import me.reeszrb.thermal.modulemanager.ModuleManager;
import me.reeszrb.thermal.override.EntityRendererHook;
import me.reeszrb.thermal.override.GuiInGame;
import me.reeszrb.thermal.override.NetHandlerPlayClientHook;
import me.reeszrb.thermal.override.PlayerController;
import me.reeszrb.thermal.playermanager.PlayerManager;
import me.reeszrb.thermal.valuemanager.ValueManager;

public class Thermal {

	public final String CLIENT_NAME = "Thermal";
	public final float CLIENT_VERSION = 1.0f;
	public final String CLIENT_TYPE = "Alpha";
	private static Thermal theThermal;
	
	
	public void initClient(){
		ConfigurationManager.getInstance();
		ModuleManager.getInstance();
		PlayerManager.getInstance();
		ValueManager.getInstance();
		Wrapper.getInstance();
		Wrapper.getInstance().getBasicUtils().printToConsole("LOADED");
		Wrapper.getInstance().getMinecraft().ingameGUI = new GuiInGame(Wrapper.getInstance().getMinecraft());
		//Wrapper.getInstance().getMinecraft().thePlayer = new EntityClientPlayer(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getMinecraft().theWorld, Wrapper.getInstance().getMinecraft().getSession(), Wrapper.getInstance().getMinecraft().getNetHandler(), Wrapper.getInstance().getPlayer().func_146107_m());
		Wrapper.getInstance().getMinecraft().playerController = new PlayerController(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getMinecraft().getNetHandler());//not working
		Wrapper.getInstance().getMinecraft().entityRenderer = new EntityRendererHook(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getMinecraft().getResourceManager());
		//Wrapper.getInstance().getMinecraft().thePlayer.sendQueue = new NetHandlerPlayClientHook(Wrapper.getInstance().getMinecraft(), Wrapper.getInstance().getMinecraft().currentScreen, Wrapper.getInstance().getMinecraft().getNetworkManager());
	}
	
	public static Thermal getInstance(){
		if(theThermal == null) theThermal = new Thermal();
		return theThermal;
	}
	
}
