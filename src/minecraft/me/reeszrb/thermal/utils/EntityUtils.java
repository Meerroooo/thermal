package me.reeszrb.thermal.utils;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.EntityEnderman;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.MathHelper;
import net.minecraft.util.MovingObjectPosition;
import net.minecraft.util.Vec3;

public class EntityUtils {
	
	private Minecraft mc = Minecraft.getMinecraft();
	
	public EntityPlayer getClosestPlayer() {
		double distance = 5000;
		EntityPlayer target = null;
		for (Object entity : mc.theWorld.playerEntities) {
			EntityPlayer e = (EntityPlayer) entity;
			if (!(e instanceof EntityPlayer) || e == mc.thePlayer) {
				continue;
			}

			if (getDistance(e.posX, e.posY, e.posZ) < distance) {
				distance = getDistance(e.posX, e.posY, e.posZ);
				target = e;
			}
		}
		return target;
	}

	public double getDistance(double X, double Y, double Z) {
		double posX = X - mc.thePlayer.posX;
		double posY = Y - mc.thePlayer.posY;
		double posZ = Z - mc.thePlayer.posZ;
		return Math.abs(Math.sqrt(posX * posX + posY * posY + posZ * posZ));
	}
	
	public void faceEntity(Entity entity){
		double x = entity.posX - mc.thePlayer.posX;
		double z = entity.posZ - mc.thePlayer.posZ;
		double y = entity.posY + (entity.getEyeHeight()/1.4D) - mc.thePlayer.posY + (mc.thePlayer.getEyeHeight()/1.4D);
		double helper = MathHelper.sqrt_double(x * x + z * z);

		float newYaw = (float)((Math.toDegrees(-Math.atan(x / z))));
		float newPitch = (float)-Math.toDegrees(Math.atan(y / helper));

		if(z < 0 && x < 0) { newYaw = (float)(90D + Math.toDegrees(Math.atan(z / x))); }
		else if(z < 0 && x > 0) { newYaw = (float)(-90D + Math.toDegrees(Math.atan(z / x))); }

		mc.thePlayer.rotationYaw = newYaw; 
		mc.thePlayer.rotationPitch = newPitch;
		mc.thePlayer.rotationYawHead = newPitch;
	}
	
	public EntityLivingBase getClosestEntity(float range) {
        EntityLivingBase returnVal = null;

        float distance = range;
        for(Entity ent :(List<Entity>) mc.theWorld.loadedEntityList) {
                if(!canAttack(ent, range)) continue;

                float curDistance = mc.thePlayer.getDistanceToEntity(ent);
                if(curDistance < distance) {
                        distance = curDistance;
                        returnVal = (EntityLivingBase) ent;
                }
        }

        return returnVal;
	}

	public boolean canAttack(Entity e, float distance) {
        	return e != mc.thePlayer &&
        			/*!FriendManager.isFriend(e.getUsername()) &&*/
        			e instanceof EntityLivingBase &&
        			!e.isInvisible() &&
        			mc.thePlayer.getDistanceToEntity(e) <= distance &&
        			e.isEntityAlive() &&
        			(canBeSeen(e) || mc.thePlayer.canEntityBeSeen(e));
	}

	public void attackEntity(Entity entity) {
        mc.thePlayer.swingItem();
        mc.playerController.attackEntity(mc.thePlayer, entity);
	}

	public float[] faceEntity(Entity entity, boolean flag) {
		double x = entity.posX - mc.thePlayer.posX;
        double z = entity.posZ - mc.thePlayer.posZ;
        double y;
        if(entity instanceof EntityEnderman) {
                y = entity.posY - mc.thePlayer.posY;
        } else {
                y = entity.posY + (entity.getEyeHeight() / 1.4D) - mc.thePlayer.posY + (mc.thePlayer.getEyeHeight() / 1.4D);
        }
        double helper = MathHelper.sqrt_double(x * x + z * z);

        float newYaw =(float)((Math.toDegrees(-Math.atan(x / z))));
        float newPitch =(float)-Math.toDegrees(Math.atan(y / helper));

        if(z < 0 && x < 0) {
                newYaw =(float)(90D + Math.toDegrees(Math.atan(z / x)));
        } else if(z < 0 && x > 0) {
                newYaw =(float)(-90D + Math.toDegrees(Math.atan(z / x)));
        }

        if(flag) {
                mc.thePlayer.rotationYaw = newYaw;
                mc.thePlayer.rotationPitch = newPitch;
        }

        return new float[] { newYaw, newPitch };
	}

	public float[] faceBlock(float blockX, float blockY, float blockZ, boolean flag) {
		double x = blockX + 0.5 - mc.thePlayer.posX;
        double z = blockZ + 0.5 - mc.thePlayer.posZ;
        double y = blockY + 0.5 - mc.thePlayer.posY;
        double helper = MathHelper.sqrt_double(x * x + z * z);

        float newYaw = (float)((Math.toDegrees(-Math.atan(x / z))));
        float newPitch = (float)-Math.toDegrees(Math.atan(y / helper));

        if(z < 0 && x < 0) {
                newYaw = (float)(90D + Math.toDegrees(Math.atan(z / x)));
        } else if(z < 0 && x > 0) {
                newYaw = (float)(-90D + Math.toDegrees(Math.atan(z / x)));
        }

        if(flag) {
                mc.thePlayer.rotationYaw += MathHelper.wrapAngleTo180_float(newYaw - mc.thePlayer.rotationYaw);
                mc.thePlayer.rotationPitch += MathHelper.wrapAngleTo180_float(newPitch - mc.thePlayer.rotationPitch);
        }
       
        return new float[] { newYaw, newPitch };
	}

	public boolean canBeSeen(Entity par1) {
        return rayTrace(mc.theWorld.getWorldVec3Pool().getVecFromPool(mc.thePlayer.posX, mc.thePlayer.posY, mc.thePlayer.posZ),
                        mc.theWorld.getWorldVec3Pool().getVecFromPool(par1.posX, par1.posY, par1.posZ)) == null;
	}

	public MovingObjectPosition rayTrace(Vec3 par1, Vec3 par2) {
		return mc.theWorld.rayTraceBlocks(par1, par2);
	}

	public double[] getRenderPosition(Entity ent) {
		return new double[] {
				ent.lastTickPosX + (ent.posX - ent.lastTickPosX) * (double) mc.getTimer().renderPartialTicks - RenderManager.renderPosX,
                ent.lastTickPosY + (ent.posY - ent.lastTickPosY) * (double) mc.getTimer().renderPartialTicks - RenderManager.renderPosY,
                ent.lastTickPosZ + (ent.posZ - ent.lastTickPosZ) * (double) mc.getTimer().renderPartialTicks - RenderManager.renderPosZ
        };
	}

	public float getYawDistanceFromEntity(Entity e) {
        double xDistance = e.posX - mc.thePlayer.posX;
        double zDistance = e.posZ - mc.thePlayer.posZ;
       
        return (float)((Math.atan2(zDistance, xDistance) * 180D) / Math.PI) - 90F;
	}

	public float getPitchDistanceFromEntity(Entity e) {
        double xDistance = e.posX - mc.thePlayer.posX;
        double yDistance = mc.thePlayer.posY + (double) mc.thePlayer.getEyeHeight() - (e.posY + (double)e.getEyeHeight());
        double zDistance = e.posZ - mc.thePlayer.posZ;
        double sqrt = (double)MathHelper.sqrt_double(xDistance * xDistance + zDistance * zDistance);
       
        return (float)-(-(Math.atan2(yDistance, sqrt) * 180.0D / Math.PI));
	}

	public boolean isDead(Entity e) {
        return !isAlive(e);
	}

	public boolean isAlive(Entity e) {
		return e != null && !e.isDead && (e instanceof EntityLivingBase ? (((EntityLivingBase) e).getHealth() >= 1) : true);
	}
}
