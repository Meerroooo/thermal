package me.reeszrb.thermal.utils;

import java.util.List;

import net.minecraft.client.Minecraft;
import net.minecraft.util.ChatComponentText;

public class BasicUtils {

	private Minecraft mc = Minecraft.getMinecraft();
	
	public void addChatMessage(String s){
		mc.thePlayer.addChatMessage(new ChatComponentText("<Thermal> " + s));
	}
	
	public long getSystemTime(){
		return System.nanoTime() / 1000000;
	}
	
	public void printToConsole(Object o){
		System.out.println("<Thermal> " + o);
	}
	
	public static boolean stringListContains(List<String> list, String needle) {
		for(String s: list) {
			if(s.trim().equalsIgnoreCase(needle.trim())) {
				return true;
			}
		}
		return false;
	}
}
