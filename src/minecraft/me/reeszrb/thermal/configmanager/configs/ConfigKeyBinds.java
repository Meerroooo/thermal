package me.reeszrb.thermal.configmanager.configs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import me.reeszrb.thermal.configmanager.Configuration;
import me.reeszrb.thermal.configmanager.ConfigurationManager;
import me.reeszrb.thermal.modulemanager.Module;
import me.reeszrb.thermal.modulemanager.ModuleManager;

import org.lwjgl.input.Keyboard;

public class ConfigKeyBinds extends Configuration{

	public static File f = new File(ConfigurationManager.getInstance().getThermalDir()  + File.separator +  "keys.txt");
	
	public ConfigKeyBinds() {
		super(f);
	}

	@Override
	public void saveConfiguration() {
		try {
			File file = f;
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			for(Module mod: ModuleManager.getInstance().getModules()) {
				if(mod.getSavable()){
					out.write(mod.getName().toLowerCase().replace(" ", "") + ":" + Keyboard.getKeyName(mod.getKeyBind()));
					out.write("\r\n");
				}
			}
			out.close();
		} catch(Exception e) {}
	}

	@Override
	public void loadConfiguration() {
		try
		{
			File file = f;
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = br.readLine()) != null)
			{
				String curLine = line.toLowerCase().trim();
				String[] s = curLine.split(":");
				String hack = s[0];
				int id = Keyboard.getKeyIndex(s[1].toUpperCase());
				for(Module mod: ModuleManager.getInstance().getModules())
				{
					if(hack.equalsIgnoreCase(mod.getName().toLowerCase().replace(" ", "")))
					{
						mod.setKeyBind(id);
					}
				}
			}
		}catch(Exception err)
		{
			err.printStackTrace();
			saveConfiguration();
		}
	}

}
