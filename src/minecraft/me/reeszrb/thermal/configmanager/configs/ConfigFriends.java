package me.reeszrb.thermal.configmanager.configs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import me.reeszrb.thermal.configmanager.Configuration;
import me.reeszrb.thermal.configmanager.ConfigurationManager;

public class ConfigFriends extends Configuration{

	public static File f = new File(ConfigurationManager.getInstance().getThermalDir() + File.separator + "friends.txt");
	
	public ConfigFriends() {
		super(f);
	}

	@Override
	public void saveConfiguration() {
		try
		{
			File file = f;
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			/*
			 * for(Friend friend: Sawary.getFriendManager().friendsList)
			{
				out.write(friend.getName() + ":" + friend.getAlias());
				out.write("\r\n");
			}
			 */
			out.close();
		}catch(Exception e) {}
	}

	@Override
	public void loadConfiguration() {
		try
		{
			File file = new File(getFile().getAbsolutePath(), getFile().getName());
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = br.readLine()) != null)
			{
				String curLine = line.trim();
				String name = curLine.split(":")[0];
				String alias = curLine.split(":")[1];
				//Sawary.getFriendManager().addFriend(name, alias);
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			saveConfiguration();
		}
	}

}
