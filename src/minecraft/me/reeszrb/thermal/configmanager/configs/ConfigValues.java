package me.reeszrb.thermal.configmanager.configs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import me.reeszrb.thermal.configmanager.Configuration;
import me.reeszrb.thermal.configmanager.ConfigurationManager;

public class ConfigValues extends Configuration{

	public static File f = new File(ConfigurationManager.getInstance().getThermalDir() + File.separator + "values.txt");
	
	public ConfigValues() {
		super(f);
	}

	@Override
	public void saveConfiguration() {
		try
		{
			File file = f;
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			//for(Value v: Value.list)
			//{
			//	out.write(v.getName() + ":" + v.getValue());
			//	out.write("\r\n");
			//}
			out.close();
		}catch(Exception e) {}
	}

	@Override
	public void loadConfiguration() {
		try
		{
			File file = new File(getFile().getAbsolutePath(), getFile().getName());
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = br.readLine()) != null)
			{
				String curLine = line.trim();
				String name = curLine.split(":")[0];
				float value = Float.parseFloat(curLine.split(":")[1]);
				//for(Value v: Value.list)
				//{
				//	if(v.getName().toLowerCase().replace(" ", "").equals(name))
				//	{
				//		v.setValue(value);
				//	}
				//}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			saveConfiguration();
		}
	}

}
