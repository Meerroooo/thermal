package me.reeszrb.thermal.configmanager.configs;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;

import me.reeszrb.thermal.configmanager.Configuration;
import me.reeszrb.thermal.configmanager.ConfigurationManager;
import me.reeszrb.thermal.modulemanager.Module;
import me.reeszrb.thermal.modulemanager.ModuleManager;

public class ConfigModules extends Configuration{

	public static File f = new File(ConfigurationManager.getInstance().getThermalDir()  + File.separator +  "hacks.txt");
	
	public ConfigModules() {
		super(f);
	}

	@Override
	public void saveConfiguration() {
		try
		{
			File file = f;
			BufferedWriter out = new BufferedWriter(new FileWriter(file));
			for(Module m: ModuleManager.getInstance().getModules()){
				if(m.getSavable()){
					out.write(m.getName().toLowerCase().replace(" ", "") + ":" + m.getState());
					out.write("\r\n");
				}
			}
					
			out.close();
		}catch(Exception e) {}
	}

	@Override
	public void loadConfiguration() {
		try
		{
			File file = f;
			FileInputStream fstream = new FileInputStream(file.getAbsolutePath());
			DataInputStream in = new DataInputStream(fstream);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			while((line = br.readLine()) != null)
			{
				String curLine = line.toLowerCase().trim();
				String name = curLine.split(":")[0];
				boolean isOn = Boolean.parseBoolean(curLine.split(":")[1]);
				for(Module mod: ModuleManager.getInstance().getModules())
				{
					if(mod.getName().toLowerCase().replace(" ", "").equals(name))
					{
						mod.setState(isOn);
					}
				}
			}
		}catch(Exception e)
		{
			e.printStackTrace();
			saveConfiguration();
		}
	}

}
