package me.reeszrb.thermal.configmanager;

import java.io.File;
import java.util.ArrayList;

import me.reeszrb.thermal.configmanager.configs.ConfigFriends;
import me.reeszrb.thermal.configmanager.configs.ConfigKeyBinds;
import me.reeszrb.thermal.configmanager.configs.ConfigModules;
import me.reeszrb.thermal.configmanager.configs.ConfigValues;
import net.minecraft.client.Minecraft;

public class ConfigurationManager {
	
	private File ThermalDir;
	private ArrayList<Configuration> config = new ArrayList<Configuration>();
	private static ConfigurationManager theConfigManager;
	
	public ConfigurationManager(){
		ThermalDir = new File(Minecraft.getMinecraft().mcDataDir + File.separator + "Thermal");
		if(!ThermalDir.exists())
		{
			ThermalDir.mkdirs();
		}
		initConfiguration();
	}
	
	public ArrayList<Configuration> getConfiguration(){
		return config;
	}
	
	public void initConfiguration(){
		getConfiguration().add(new ConfigFriends());
		getConfiguration().add(new ConfigKeyBinds());
		getConfiguration().add(new ConfigModules());
		getConfiguration().add(new ConfigValues());
	}
	
	public File getThermalDir(){
		return ThermalDir;
	}
	
	public Configuration getConfig(Class<?extends Configuration> clazz) 
	{
		for(Configuration config: getConfiguration())
		{
			if(config.getClass().equals(clazz))
			{
				return config;
			}
		}
		return null;
	}
	
	public static ConfigurationManager getInstance(){
		if(theConfigManager == null) theConfigManager = new ConfigurationManager();
		return theConfigManager;
	}
	
}
