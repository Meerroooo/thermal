package me.reeszrb.thermal.configmanager;

import java.io.File;
import java.io.IOException;

public abstract class Configuration {
	public File f;
	
	public Configuration(File location){
		f = location;
			
		loadConfiguration();
	}
	
	public abstract void saveConfiguration();
	
	public abstract void loadConfiguration();
	
	public File getFile(){
		return f;
	}
}
